<ui>
  <menubar name='Main' action='MainMenu'>
    <menu name='Session' action='Session'>
      <menuitem action='New'/>
      <menuitem action='Open'/>
      <menuitem action='Recent'/>
      <menuitem action='Close'/>
      <separator/>
      <menuitem action='Save'/>
      <menuitem action='SaveAs'/>
      <separator/>
      <menuitem action='SaveTemplate'/>
      <separator/>
      <menuitem action='addExistingAudioFiles'/>
      <menu name='Export' action='Export'>
        <menuitem action='QuickExport'/>
        <menuitem action='StemExport'/>
      </menu>
#ifdef __APPLE__
      <menuitem action='toggle-about'/>
#endif
      <separator/>
      <menuitem action='toggle-session-options-editor'/>
      <separator/>
      <menuitem action='lock'/>
#ifndef __APPLE__
      <separator/>
      <menuitem action='Quit'/>
#endif
    </menu>

    <menu name='Transport' action='Transport'>
      <menuitem action='ToggleRoll'/>
      <menuitem action='Record'/>
      <separator/>
      <menuitem action='crash-record'/>
      <separator/>
      <menu action="MovePlayHeadMenu">
        <menuitem action='set-playhead'/>
        <menuitem action='GotoStart'/>
        <menuitem action='GotoEnd'/>
      </menu>
      <separator/>
        <menuitem action='add-location-from-playhead'/>
        <menuitem action='add-section-from-playhead'/>
        <menuitem action='jump-forward-to-mark'/>
        <menuitem action='jump-backward-to-mark'/>
      <separator/>
      <separator/>
      <menuitem action='ToggleFollowEdits'/>
      <menuitem action='ToggleAutoReturn'/>
      <separator/>
    </menu>

    <menu name='Edit' action='Edit'>
      <menuitem action='undo'/>
      <menuitem action='redo'/>
      <separator/>
      <menuitem action='editor-cut'/>
      <menuitem action='editor-copy'/>
      <menuitem action='editor-paste'/>
      <menuitem action='editor-delete'/>
      <menuitem action='editor-crop'/>
      <separator/>
      <menuitem action='solo-selection'/>
      <separator/>
      <menuitem action='select-loop-range'/>
      <separator/>
      <menuitem action='split-region'/>
        <separator/>
        <menuitem action='start-range'/>
        <menuitem action='finish-range'/>
        <separator/>
     <menuitem action='remove-last-capture'/>
    </menu>

    <menu name='RegionMenu' action='RegionMenu'>
       <menuitem action='select-all-objects'/>
       <separator/>
       <menuitem action='toggle-region-mute'/>
         <menuitem action='normalize-region'/>
         <menuitem action='boost-region-gain'/>
         <menuitem action='cut-region-gain'/>
         <menuitem action='reset-region-gain'/>
         <menuitem action='toggle-region-lock'/>
    </menu>

    <menu action='TrackMenu'>
      <menuitem action='AddTrackBus'/>
      <menuitem action='remove-track'/>
      <menuitem action="move-selected-tracks-up"/>
      <menuitem action="move-selected-tracks-down"/>
    </menu>

    <menu name='View' action = 'View'>
      <menu action="PrimaryClockMenu">
        <menuitem action="primary-clock-timecode"/>
        <menuitem action="primary-clock-minsec"/>
        <menuitem action="primary-clock-samples"/>
     </menu>
     <menu action="ZoomMenu">
        <menuitem action="fit-selection"/>
        <menuitem action="zoom-to-selection"/>
        <menuitem action='temporal-zoom-in'/>
        <menuitem action='temporal-zoom-out'/>
        <menuitem action='zoom-to-session'/>
      </menu>
   </menu>

    <menu action = 'WindowMenu'>
     <menuitem action='toggle-audio-midi-setup'/>
    <separator/>
     <menuitem action='toggle-transport-masters'/>
    <separator/>
     <menuitem action='toggle-meterbridge'/>
     <menuitem action='toggle-big-clock'/>
     <menuitem action='toggle-big-transport'/>
    <separator/>
     <menuitem action='toggle-audio-connection-manager'/>
     <menuitem action='toggle-midi-connection-manager'/>
    <separator/>
     <menuitem action='NewMIDITracer'/>
    <separator/>
     <menuitem action='toggle-about'/>
    </menu>
</menubar>

  <popup action="RulerMenuPopup" accelerators='true'>
    <menuitem action="toggle-minsec-ruler"/>
    <menuitem action="toggle-timecode-ruler"/>
    <menuitem action="toggle-samples-ruler"/>
  </popup>

  <popup name='ProcessorMenu' accelerators='true'>
    <menuitem action='presets'/>
    <menuitem action='edit'/>
    <menuitem action='edit-generic'/>
    <menuitem action='controls'/>
    <menuitem action='rename'/>
    <separator/>
    <menuitem action='manage-pins'/>
    <separator/>
    <menuitem action='send_options'/>
    <separator/>
    <menuitem action='newplugin'/>
    <menuitem action='newinsert'/>
    <menuitem action='newsend'/>
    <menuitem action='newaux'/>
    <menuitem action='newlisten'/>
    <menuitem action='removelisten'/>
    <separator/>
    <menuitem action='clear'/>
    <menuitem action='clear_pre'/>
    <menuitem action='clear_post'/>
    <separator/>
    <menuitem action='cut'/>
    <menuitem action='copy'/>
    <menuitem action='paste'/>
    <menuitem action='delete'/>
    <separator/>
    <menuitem action='selectall'/>
    <menuitem action='deselectall'/>
    <separator/>
    <menuitem action='activate_all'/>
    <menuitem action='deactivate_all'/>
#ifndef MIXBUS
    <menuitem action='ab_plugins'/>
#endif
    <separator/>
#ifndef MIXBUS
    <menu action="disk-io-menu">
      <menuitem action='disk-io-prefader'/>
      <menuitem action='disk-io-postfader'/>
      <menuitem action='disk-io-custom'/>
    </menu>
#endif
    <menuitem action='custom-volume-pos'/>
    <separator/>
  </popup>

  <popup name='ShuttleUnitPopup' accelerators='true'>
    <menuitem action='SetShuttleUnitsPercentage'/>
    <menuitem action='SetShuttleUnitsSemitones'/>
  </popup>

  <popup name='RegionListMenu' accelerators='true'>
    <menuitem action='rlAudition'/>
    <separator/>
    <menuitem action='addExternalAudioToRegionList'/>
    <separator/>
    <menuitem action='removeUnusedRegions'/>
  </popup>

<popup name='RangeShowMenu' accelerators='true'>
   <menuitem action='show-all-ranges'/>
   <menuitem action='show-session-range'/>
   <menuitem action='show-punch-range'/>
   <menuitem action='show-loop-range'/>
   <menuitem action='show-other-ranges'/>
</popup>

<popup name='MarkerShowMenu' accelerators='true'>
   <menuitem action='show-all-markers'/>
   <menuitem action='show-cd-markers'/>
   <menuitem action='show-cue-markers'/>
   <menuitem action='show-scene-markers'/>
   <menuitem action='show-location-markers'/>
</popup>

  <popup name='PopupRegionMenu' action='PopupRegionMenu' accelerators='true'>
    <menuitem action='group-selected-regions'/>
    <menuitem action='ungroup-selected-regions'/>
    <separator/>
    <menuitem action='toggle-region-lock'/>
    <separator/>
    <menu action='RegionMenuGain'>
      <menuitem action='toggle-region-mute'/>
      <menuitem action='normalize-region'/>
      <menuitem action='boost-region-gain'/>
      <menuitem action='cut-region-gain'/>
      <menuitem action='reset-region-gain'/>
      <menuitem action='toggle-region-polarity'/>
    </menu>
    <separator/>
    <menuitem action='export-region'/>
    <separator/>
    <menuitem action='loudness-analyze-region'/>
    <menuitem action='spectral-analyze-region'/>
    <separator/>
    <menuitem action='remove-region'/>
  </popup>

</ui>
