/*
 * Copyright (C) 2005-2019 Paul Davis <paul@linuxaudiosystems.com>
 * Copyright (C) 2005 Taybin Rutkin <taybin@taybin.com>
 * Copyright (C) 2006-2015 Tim Mayberry <mojofunk@gmail.com>
 * Copyright (C) 2007-2011 David Robillard <d@drobilla.net>
 * Copyright (C) 2009-2012 Carl Hetherington <carl@carlh.net>
 * Copyright (C) 2013-2018 Robin Gareus <robin@gareus.org>
 * Copyright (C) 2016-2018 Ben Loftis <ben@harrisonconsoles.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifdef WAF_BUILD
#include "gtk2ardour-config.h"
#endif

/* this file exists solely to break compilation dependencies that
   would connect changes to the mixer or editor objects.
*/

#include <cstdio>

#include "pbd/error.h"

#include "ardour/session.h"
#include "ardour/lv2_plugin.h"
#include "ardour/profile.h"

#include "gtkmm2ext/bindings.h"

#include "widgets/ardour_button.h"
#include "widgets/tooltips.h"

#include "actions.h"
#include "application_bar.h"
#include "ardour_message.h"
#include "ardour_ui.h"
#include "audio_clip_editor.h"
#include "public_editor.h"
#include "meterbridge.h"
#include "luainstance.h"
#include "luawindow.h"
#include "livetrax_meters.h"
#include "main_clock.h"
#include "meterbridge.h"
#include "mixer_ui.h"
#include "recorder_ui.h"
#include "trigger_page.h"
#include "keyboard.h"
#include "keyeditor.h"
#include "rc_option_editor.h"
#include "route_params_ui.h"
#include "selection_properties_box.h"
#include "trigger_ui.h"
#include "step_entry.h"
#include "opts.h"

#ifdef GDK_WINDOWING_X11
#include <ydk/gdkx.h>
#endif

#include "pbd/i18n.h"

using namespace Gtk;
using namespace PBD;
using namespace ArdourWidgets;

namespace ARDOUR {
	class Session;
	class Route;
}

using namespace ARDOUR;
using namespace Gtkmm2ext;

void
ARDOUR_UI::we_have_dependents ()
{
	install_dependent_actions ();

	/* The monitor section relies on at least 1 action defined by us. Since that
	 * action now exists, give it a chance to use it.
	 */
	mixer->monitor_section().use_others_actions ();

	StepEntry::setup_actions_and_bindings ();
	ClipEditorBox::init ();

	/* Global, editor, mixer, processor box actions are defined now. Link
	   them with any bindings, so that GTK does not get a chance to define
	   the GTK accel map entries first when we ask the GtkUIManager to
	   create menus/widgets.

	   If GTK adds the actions to its accel map before we do, we lose our
	   freedom to use any keys. More precisely, we can use any keys, but
	   ones that GTK considers illegal as accelerators will not show up in
	   menus.

	   There are other dynamic actions that can be created by a monitor
	   section, by step entry dialogs. These need to be handled
	   separately. They don't tend to use GTK-illegal bindings and more
	   importantly they don't have menus showing the bindings, so it is
	   less of an issue.
	*/

	Gtkmm2ext::Bindings::associate_all ();

	editor->UpdateAllTransportClocks.connect (sigc::mem_fun (*this, &ARDOUR_UI::update_transport_clocks));

	/* all actions are defined */

	ActionManager::load_menus (ARDOUR_COMMAND_LINE::menus_file);

	/* catch up on parameters */

	std::function<void (std::string)> pc (std::bind (&ARDOUR_UI::parameter_changed, this, _1));
	Config->map_parameters (pc);
}

void
ARDOUR_UI::connect_dependents_to_session (ARDOUR::Session *s)
{
	DisplaySuspender ds;
	BootMessage (_("Setup Editor"));
	editor->set_session (s);
	_livetrax_sections->set_session (s); // XXX
	BootMessage (_("Setup Mixer"));
	mixer->set_session (s);
	meterbridge->set_session (s);

	if (!Profile->get_livetrax()) {
		recorder->set_session (s);
		trigger_page->set_session (s);
	}

	/* its safe to do this now */

	BootMessage (_("Reload Session History"));
	s->restore_history ("");
}

/** The main editor window has been closed */
gint
ARDOUR_UI::exit_on_main_window_close (GdkEventAny * /*ev*/)
{
#ifdef __APPLE__
	/* just hide the window, and return - the top menu stays up */
	editor->hide ();
	return TRUE;
#else
	/* time to get out of here */
	finish();
	return TRUE;
#endif
}

GtkNotebook*
ARDOUR_UI::tab_window_root_drop (GtkNotebook* src,
				 GtkWidget* w,
				 gint x,
				 gint y,
				 gpointer)
{
	using namespace std;
	Gtk::Notebook* nb = 0;
	Gtk::Window* win = 0;
	ArdourWidgets::Tabbable* tabbable = 0;


	if (w == GTK_WIDGET(editor->contents().gobj())) {
		tabbable = editor;
	} else if (w == GTK_WIDGET(mixer->contents().gobj())) {
		tabbable = mixer;
#ifndef LIVETRAX
	} else if (w == GTK_WIDGET(rc_option_editor->contents().gobj())) {
		tabbable = rc_option_editor;
#endif
	} else if (w == GTK_WIDGET(recorder->contents().gobj())) {
		tabbable = recorder;
	} else if (w == GTK_WIDGET(trigger_page->contents().gobj())) {
		tabbable = trigger_page;
	} else {
		return 0;
	}

	nb = tabbable->tab_root_drop ();
	win = tabbable->own_window ();

	if (nb) {
		win->move (x, y);
		win->show_all ();
		win->present ();
		return nb->gobj();
	}

	return 0; /* what was that? */
}

bool
ARDOUR_UI::idle_ask_about_quit ()
{
	const auto ask_before_closing = UIConfiguration::instance ().get_ask_before_closing_last_window ();

	if ((_session && _session->dirty ()) || !ask_before_closing) {
		finish ();
	} else {
		/* no session or session not dirty, but still ask anyway */

		ArdourMessageDialog msg (string_compose (_("Quit %1?"), PROGRAM_NAME),
		                         false, /* no markup */
		                         Gtk::MESSAGE_INFO,
		                         Gtk::BUTTONS_YES_NO,
		                         true); /* modal */
		msg.set_default_response (Gtk::RESPONSE_YES);
		msg.set_position (WIN_POS_MOUSE);

		if (msg.run () == Gtk::RESPONSE_YES) {
			finish ();
		}
	}

	/* not reached but keep the compiler happy */

	return false;
}

bool
ARDOUR_UI::main_window_delete_event (GdkEventAny* ev)
{
	/* quit the application as soon as we go idle. If we call this here,
	 * the window manager/desktop can think we're taking too longer to
	 * handle the "delete" event
	 */

	Glib::signal_idle().connect (sigc::mem_fun (*this, &ARDOUR_UI::idle_ask_about_quit));

	return true;
}

static GtkNotebook*
tab_window_root_drop (GtkNotebook* src,
                      GtkWidget* w,
                      gint x,
                      gint y,
                      gpointer user_data)
{
	return ARDOUR_UI::instance()->tab_window_root_drop (src, w, x, y, user_data);
}

int
ARDOUR_UI::setup_windows ()
{
	if (ARDOUR::Profile->get_livetrax()) {
		return livetrax_setup_windows ();
	}

	_tabs.set_show_border(false);
	_tabs.signal_switch_page().connect (sigc::mem_fun (*this, &ARDOUR_UI::tabs_switch));
	_tabs.signal_page_added().connect (sigc::mem_fun (*this, &ARDOUR_UI::tabs_page_added));
	_tabs.signal_page_removed().connect (sigc::mem_fun (*this, &ARDOUR_UI::tabs_page_removed));

	rc_option_editor = new RCOptionEditor;
#ifndef LIVETRAX
	rc_option_editor->StateChange.connect (sigc::mem_fun (*this, &ARDOUR_UI::tabbable_state_change));
#endif

	if (create_editor ()) {
		error << _("UI: cannot setup editor") << endmsg;
		return -1;
	}

	if (create_mixer ()) {
		error << _("UI: cannot setup mixer") << endmsg;
		return -1;
	}

	if (create_recorder ()) {
		error << _("UI: cannot setup recorder") << endmsg;
		return -1;
	}

	if (create_trigger_page ()) {
		error << _("UI: cannot setup trigger") << endmsg;
		return -1;
	}

	if (create_meterbridge ()) {
		error << _("UI: cannot setup meterbridge") << endmsg;
		return -1;
	}

	/* all other dialogs are created conditionally */

	we_have_dependents ();

	/* order of addition affects order seen in initial window display */

#ifndef LIVETRAX
	rc_option_editor->add_to_notebook (_tabs);
#endif
	mixer->add_to_notebook (_tabs);
	editor->add_to_notebook (_tabs);
	recorder->add_to_notebook (_tabs);
	trigger_page->add_to_notebook (_tabs);

	top_packer.pack_start (menu_bar_base, false, false);

	main_vpacker.pack_start (top_packer, false, false);

	ArdourWidgets::ArdourDropShadow *spacer = manage (new (ArdourWidgets::ArdourDropShadow));
	spacer->set_size_request( -1, 4 );
	spacer->show();

	/* now add the transport sample to the top of main window */

	main_vpacker.pack_start ( *spacer, false, false);
	main_vpacker.pack_start (_tabs, true, true);

	setup_transport();
	build_menu_bar ();
	setup_tooltips ();

	/* set DPI before realizing widgets */
	UIConfiguration::instance().reset_dpi ();

	ActionsReady (); // EMIT SIGNAL

	_main_window.signal_delete_event().connect (sigc::mem_fun (*this, &ARDOUR_UI::main_window_delete_event));

	/* pack the main vpacker into the main window and show everything
	 */

	_main_window.add (main_vpacker);

	apply_window_settings (true);

	setup_toplevel_window (_main_window, "", this);
	_main_window.show_all ();

	_tabs.set_show_tabs (false);

	/* It would be nice if Gtkmm had wrapped this rather than just
	 * deprecating the old set_window_creation_hook() method, but oh well...
	 */
	g_signal_connect (_tabs.gobj(), "create-window", (GCallback) ::tab_window_root_drop, this);

#ifdef GDK_WINDOWING_X11
	/* allow externalUIs to be transient, on top of the main window */
	LV2Plugin::set_main_window_id (GDK_DRAWABLE_XID(_main_window.get_window()->gobj()));
#endif

	return 0;
}

int
ARDOUR_UI::livetrax_setup_windows ()
{
	using namespace Menu_Helpers;
	using namespace Gtk;

	livetrax_app_bar = new ApplicationBar;

	rc_option_editor = new RCOptionEditor;
	rc_option_editor->hide();

	ArdourButton::set_default_tweaks (ArdourButton::Tweaks (ArdourButton::ForceFlat));

	if (create_editor()) {
		error << _("UI: cannot setup editor") << endmsg;
		return -1;
	}

	if (create_mixer()) {
		error << _("UI: cannot setup mixer") << endmsg;
		return -1;
	}

	if (create_meterbridge ()) {
		error << _("UI: cannot setup meterbridge") << endmsg;
		return -1;
	}

	_livetrax_sections = new EditorSections();

	ArdourButton::Element elements (ArdourButton::Element (ArdourButton::Text|ArdourButton::VectorIcon));
	Glib::RefPtr<Action> act;

	livetrax_view_in_folder_button = manage (new ArdourButton (elements));
	livetrax_view_in_folder_button->set_icon (ArdourWidgets::ArdourIcon::Folder);
	ArdourWidgets::set_tooltip (livetrax_view_in_folder_button, _("Show Files in your file manager"));

	livetrax_editor_view_button = manage (new ArdourButton (_("TRACKS"), ArdourButton::Element (ArdourButton::Text|ArdourButton::Body), true));
	livetrax_editor_view_button->set_icon (ArdourWidgets::ArdourIcon::TrackWaveform);
	act = ActionManager::get_action (X_("Common"), X_("livetrax-toggle-editor"));
	livetrax_editor_view_button->set_related_action (act);
	livetrax_editor_view_button->set_name ("page switch button");
	ArdourWidgets::set_tooltip (livetrax_editor_view_button, _("Show/Hide Track display"));

	livetrax_prefs_view_button = manage (new ArdourButton (_("PREFS"), ArdourButton::Element (ArdourButton::Text|ArdourButton::Body), true));
	livetrax_prefs_view_button->set_icon (ArdourWidgets::ArdourIcon::Config);
	act = ActionManager::get_action (X_("Common"), X_("show-ui-prefs"));
	livetrax_prefs_view_button->set_related_action (act);
	livetrax_prefs_view_button->set_name ("page switch button");
	ArdourWidgets::set_tooltip (livetrax_prefs_view_button, _("Show/Hide Preferences window"));

	livetrax_meter_view_button = manage (new ArdourButton (ArdourButton::Element (ArdourButton::Text|ArdourButton::Body), true));;
	livetrax_meter_view_button->set_icon (ArdourWidgets::ArdourIcon::Meters);
	act = ActionManager::get_action (X_("Common"), X_("livetrax-toggle-meter"));
	livetrax_meter_view_button->set_related_action (act);
	livetrax_meter_view_button->set_name ("page switch button");
	ArdourWidgets::set_tooltip (livetrax_meter_view_button, _("Show/Hide Soundcard input meters"));

	livetrax_mixer_view_button = manage (new ArdourButton (ArdourButton::Element (ArdourButton::Text|ArdourButton::Body), true));
	livetrax_mixer_view_button->set_icon (ArdourWidgets::ArdourIcon::Mixer);
	act = ActionManager::get_action (X_("Common"), X_("livetrax-toggle-mixer"));
	livetrax_mixer_view_button->set_related_action (act);
	livetrax_mixer_view_button->set_name ("page switch button");
	ArdourWidgets::set_tooltip (livetrax_mixer_view_button, _("Show/Hide Mixer"));

	livetrax_lock_button = manage (new ArdourButton (elements, true));
	livetrax_lock_button->set_icon (ArdourWidgets::ArdourIcon::Lock);
	act = ActionManager::get_action (X_("Editor"), X_("lock"));
	livetrax_lock_button->set_related_action (act);
	ArdourWidgets::set_tooltip (livetrax_lock_button, _("Lock the GUI"));

	int TCOL = 0;
	tabbables_table.attach (*livetrax_lock_button,           TCOL,   TCOL+1, 0, 1, FILL, FILL, 4, 4);
	tabbables_table.attach (*livetrax_view_in_folder_button, TCOL+1, TCOL+2, 0, 1, FILL, FILL, 4, 4);
	tabbables_table.attach (*livetrax_prefs_view_button,     TCOL+2, TCOL+3, 0, 1, FILL, FILL, 4, 4);
	tabbables_table.attach (*livetrax_meter_view_button,     TCOL,   TCOL+1, 1, 2, FILL, FILL, 4, 4);
	tabbables_table.attach (*livetrax_editor_view_button,    TCOL+1, TCOL+2, 1, 2, FILL, FILL, 4, 4);
	tabbables_table.attach (*livetrax_mixer_view_button,     TCOL+2, TCOL+3, 1, 2, FILL, FILL, 4, 4);
	tabbables_table.show_all ();

	button_height_size_group->add_widget (*livetrax_lock_button);
	button_height_size_group->add_widget (*livetrax_mixer_view_button);
	button_height_size_group->add_widget (*primary_clock);

	//sync all the view buttons so they look squarer
	_livetrax_viewbutton_sizegroup->add_widget(*livetrax_lock_button);
	_livetrax_viewbutton_sizegroup->add_widget(*livetrax_view_in_folder_button);
	_livetrax_viewbutton_sizegroup->add_widget(*livetrax_prefs_view_button);
	_livetrax_viewbutton_sizegroup->add_widget(*livetrax_meter_view_button);
	_livetrax_viewbutton_sizegroup->add_widget(*livetrax_editor_view_button);
	_livetrax_viewbutton_sizegroup->add_widget(*livetrax_mixer_view_button);

	/* meter display */

	livetrax_meters = manage (new LiveTraxMeters);
	livetrax_meters->show_all ();
	livetrax_meter_bar.pack_start (*livetrax_meters, true, true, 12);

	HBox* hb = manage (new HBox);
	livetrax_edit_vscrollbar = manage (new VScrollbar (editor->vertical_adjustment));
	livetrax_edit_vscrollbar->show ();
	hb->pack_start (editor->contents(), true, true);
	hb->pack_start (*livetrax_edit_vscrollbar, false, false);

	livetrax_section_pane.set_child_minsize(_livetrax_sections->widget(), 30);
	livetrax_section_pane.set_divider(0, 0.99);
	livetrax_section_pane.add(*hb);
	livetrax_section_pane.add (_livetrax_sections->widget());

	VBox* vb = manage (new VBox);
	livetrax_edit_hscrollbar = manage (new HScrollbar (editor->horizontal_adjustment));
	livetrax_edit_hscrollbar->show ();
	vb->pack_start (livetrax_section_pane, true, true);

	livetrax_editor_bar.pack_start (*vb, true, true, 12);

	livetrax_section_pane.set_divider(0, 0.99);

	livetrax_mixer_bar.pack_start (mixer->contents(), true, true, 12);

	we_have_dependents ();

	editor->livetrax_assign_ui_dependent_actions();

	_livetrax_spacer1.set_size_request(-1, 6);
	_livetrax_spacer2.set_size_request(-1, 6);
	_livetrax_spacer3.set_size_request(-1, 12);
	_livetrax_spacer4.set_size_request(-1, 12);

	/* order of addition affects order seen in initial window display */

	main_vpacker.pack_start (menu_bar_base, false, false);
	main_vpacker.pack_start (_livetrax_spacer1, false, false);
	main_vpacker.pack_start (*livetrax_app_bar, false, false);
	main_vpacker.pack_start (_livetrax_spacer2, false, false);
	main_vpacker.pack_start (livetrax_meter_bar, false, false);
	main_vpacker.pack_start (_livetrax_spacer3, false, false);
	main_vpacker.pack_start (livetrax_editor_bar, true, true);
	main_vpacker.pack_start (_livetrax_spacer4, false, false);
	main_vpacker.pack_start (livetrax_mixer_bar, false, false);

	setup_tooltips ();
	build_menu_bar ();

	disk_space_label.set_alignment (0.0, 0.5);
	dsp_load_label.set_alignment (0.0, 0.5);

	Gtkmm2ext::Bindings* bindings;

	bindings = Bindings::get_bindings (X_("Editor"));
	livetrax_editor_bar.set_data ("ardour-bindings", bindings);
	bindings = Bindings::get_bindings (X_("Mixer"));
	livetrax_mixer_bar.set_data ("ardour-bindings", bindings);

	// setup_tooltips ();

	ActionsReady (); // EMIT SIGNAL

	act = ActionManager::get_action (X_("Common"), X_("open-media-folder"));
	livetrax_view_in_folder_button->set_related_action (act);

	error_alert_button.signal_button_release_event().connect (sigc::mem_fun(*this,&ARDOUR_UI::error_alert_press), false);
	act = ActionManager::get_action (X_("Editor"), X_("toggle-log-window"));
	error_alert_button.set_related_action(act);
	error_alert_button.set_fallthrough_to_parent(true);

	_main_window.signal_delete_event().connect (sigc::mem_fun (*this, &ARDOUR_UI::main_window_delete_event));

	/* pack the main vpacker into the main window and show everything
	 */

	_main_window.add (main_vpacker);

	apply_window_settings (true);

	setup_toplevel_window (_main_window, "", this);
	_main_window.show_all ();

	_livetrax_visibility = LiveTraxVisibility (LiveTraxMeterVisible|LiveTraxEditorVisible);
	livetrax_visibility_change ();

#ifdef GDK_WINDOWING_X11
	/* allow externalUIs to be transient, on top of the main window */
	LV2Plugin::set_main_window_id (GDK_DRAWABLE_XID(_main_window.get_window()->gobj()));
#endif

	return 0;
}

void
ARDOUR_UI::apply_window_settings (bool with_size)
{
	const XMLNode* mnode = main_window_settings ();

	if (!mnode) {
		return;
	}

	XMLProperty const* prop;

	if (with_size) {
		gint x = -1;
		gint y = -1;
		gint w = -1;
		gint h = -1;

		if ((prop = mnode->property (X_("x"))) != 0) {
			x = atoi (prop->value());
		}

		if ((prop = mnode->property (X_("y"))) != 0) {
			y = atoi (prop->value());
		}

		if ((prop = mnode->property (X_("w"))) != 0) {
			w = atoi (prop->value());
		}

		if ((prop = mnode->property (X_("h"))) != 0) {
			h = atoi (prop->value());
		}

		if (x >= 0 && y >= 0 && w >= 0 && h >= 0) {
			_main_window.set_position (Gtk::WIN_POS_NONE);
		}

		if (x >= 0 && y >= 0) {
			_main_window.move (x, y);
		}

		if (w > 0 && h > 0) {
			_main_window.set_default_size (w, h);
		}
	}

	std::string current_tab;

	if ((prop = mnode->property (X_("current-tab"))) != 0) {
		current_tab = prop->value();
	} else {
		current_tab = "editor";
	}

	if (mixer && current_tab == "mixer") {
		_tabs.set_current_page (_tabs.page_num (mixer->contents()));
#ifndef LIVETRAX
	} else if (rc_option_editor && current_tab == "preferences") {
		_tabs.set_current_page (_tabs.page_num (rc_option_editor->contents()));
#endif
	} else if (recorder && current_tab == "recorder") {
		_tabs.set_current_page (_tabs.page_num (recorder->contents()));
	} else if (trigger_page && current_tab == "trigger") {
		_tabs.set_current_page (_tabs.page_num (trigger_page->contents()));
	} else if (editor) {
		_tabs.set_current_page (_tabs.page_num (editor->contents()));
	}
	return;
}

void
ARDOUR_UI::update_section_visibility(bool show)
{
#ifdef LIVETRAX
	if (!_livetrax_sections) {
		return;
	}
	if (show) {
		_livetrax_sections->widget().show();
	} else {
		_livetrax_sections->widget().hide();
	}
#endif
}

void
ARDOUR_UI::livetrax_visibility_change ()
{
	if (_livetrax_visibility & LiveTraxMeterVisible) {
		livetrax_meter_bar.show ();
		_livetrax_spacer2.show();
		livetrax_meter_view_button->set_active_state (Gtkmm2ext::ExplicitActive);
	} else {
		livetrax_meter_bar.hide ();
		_livetrax_spacer2.hide();
		livetrax_meter_view_button->set_active_state (Gtkmm2ext::Off);
	}

	if (_livetrax_visibility & LiveTraxEditorVisible) {
		livetrax_editor_bar.show ();
		_livetrax_spacer3.show();
		livetrax_editor_view_button->set_active_state (Gtkmm2ext::ExplicitActive);
	} else {
		livetrax_editor_bar.hide ();
		_livetrax_spacer3.hide();
		livetrax_editor_view_button->set_active_state (Gtkmm2ext::Off);
	}

	if (_livetrax_visibility & LiveTraxMixerVisible) {
		livetrax_mixer_bar.show ();
		_livetrax_spacer4.show();
		livetrax_mixer_view_button->set_active_state (Gtkmm2ext::ExplicitActive);
	} else {
		livetrax_mixer_bar.hide ();
		_livetrax_spacer4.hide();
		livetrax_mixer_view_button->set_active_state (Gtkmm2ext::Off);
	}
}

void
ARDOUR_UI::livetrax_toggle_visibility (LiveTraxVisibility v)
{
	if (_livetrax_visibility & v) {
		_livetrax_visibility = LiveTraxVisibility (_livetrax_visibility & ~v);
	} else {
		_livetrax_visibility = LiveTraxVisibility (_livetrax_visibility | v);
	}

	if (_livetrax_visibility==0) { //at minimum always show tracks
		_livetrax_visibility = LiveTraxVisibility (LiveTraxEditorVisible);
	}

	livetrax_visibility_change ();
}

void
ARDOUR_UI::livetrax_set_file_format (LiveTraxFileFormat const & ff)
{
	if (!_session) {
		return;
	}

	/* bail out if any track is armed */
	std::shared_ptr<RouteList> rl = _session->get_tracks ();
	for (RouteList::iterator r = rl->begin(); r != rl->end(); ++r) {
		std::shared_ptr<Track> t = std::dynamic_pointer_cast<Track> (*r);
		assert (t);
		if (t->rec_enable_control()->get_value()) {
			return;
		}
	}

	/* Don't reset write sources on header format change */

	_session->disable_file_format_reset ();

	_session->config.set_native_file_header_format (ff.hf);
	_session->config.set_native_file_data_format (ff.sf);

	_session->enable_file_format_reset ();

	_session->reset_native_file_format ();
}
