#ifndef __ardour_gtk_livetrax_meters_h__
#define __ardour_gtk_livetrax_meters_h__

#include <unistd.h>

#include <ytkmm/box.h>
#include <ytkmm/scrolledwindow.h>

#include "pbd/signals.h"

namespace Gtk {
	class Label;
}

namespace ArdourWidgets {
	class FastMeter;
}

class LiveTraxMeters : public Gtk::ScrolledWindow
{
public:
	LiveTraxMeters ();
	~LiveTraxMeters ();

private:
	void start_updating ();
	void stop_updating ();
	bool update_meters ();
	void resize (size_t);

	Gtk::HBox                              _meter_box;
	std::vector<Gtk::Widget*>              _widgets;
	std::vector<ArdourWidgets::FastMeter*> _meters;
	PBD::ScopedConnectionList              _engine_connections;
	sigc::connection                       _fast_screen_update_connection;
};

#endif /* __ardour_gtk_livetrax_meters_h__ */
